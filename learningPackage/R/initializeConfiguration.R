#
#
# aktana-learning model building code for Aktana Learning Engines.
#
# description: This for extracting configuration info
#
#
# created by : marc.cohen@aktana.com
#
# created on : 2015-10-13
#
# Copyright AKTANA (c) 2015.
#
##########################################################

initializeConfiguration <- function()
{
    library(properties)
    
    # get properties 
    propertiesFilePath <- sprintf("%s/builds/%s/learning.properties",homedir,BUILD_UID);
    CONFIGURATION <<- read.properties(propertiesFilePath)
    CATALOGENTRY <<- sprintf("%s %s --- %s -- %s",CATALOGENTRY,runStamp,DRIVERMODULE,BUILD_UID)
    RunTimeStamp <- sprintf("Run initialized at: %s",Sys.time())
    flog.info(RunTimeStamp)
    addWorksheet(WORKBOOK,"RunTimeStamp")
    writeData(WORKBOOK,"RunTimeStamp",RunTimeStamp)
    flog.info("Return from initConfiguration")
}

