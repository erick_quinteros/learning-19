# Update python path to find DataAccessLayer
import os
import sys

# Update sys path to find the modules from Common and DataAccessLayer
script_path = os.path.realpath(__file__)
script_dir = os.path.dirname(script_path)
learning_dir = os.path.dirname(script_dir)
sys.path.append(learning_dir)

from common.DataAccessLayer.DataAccessLayer import SegmentAccessor
from common.DataAccessLayer.DataAccessLayer import DatabaseIntializer
from common.DataAccessLayer.DatabaseConfig import DatabaseConfig
from common.DataAccessLayer.DataAccessLayer import LearningAccessor
from common.DataAccessLayer.DataAccessLayer import DSEAccessor
from sparkSimulationMessageSequence.LearningPropertiesReader import LearningPropertiesReader
from sparkSimulationMessageSequence.SimulationMessageSequenceScore import MessageSequencePredictor
from sparkSimulationMessageSequence.LearningPropertiesKey import LearningPropertiesKey
from sparkSimulationMessageSequence.SimulationSegementScoreAggregator import SimulationScoreAggregator
import datetime
from sparkSimulationMessageSequence.SimulationLogger import create_logger
import common.DataAccessLayer.DataAccessLayer as data_access_layer
import logging
import pandas as pd
import time
import threading
from pyspark.sql import SparkSession
import uuid
from common.DataAccessLayer.SparkDataAccessLayer import initialize

LEARNING_PROPERTIES_FILE_NAME = "learning.properties"
LEARNING_DB_SUFFIX = "_learning"
MAX_SEGMENT_FEATURE_VALUE_NUM = 500

SIMULATION_EXEC_RUNNING_STATUS = "running"
SIMULATION_EXEC_SUCCESS_STATUS = "success"
SIMULATION_EXEC_FAILURE_STATUS = "failure"

SQL_TIMESTAMP_FORMAT = "%Y-%m-%d %H:%M:%S"

# Refresh rate in days for the `SimulationAccountEmailSent` table in learning database
SIM_ACCOUNT_SENT_EMAIL_REFRESH_RATE = 1

# Refresh rate in days for the `ApprovedDocuments` table in learning database
SIM_APPROVED_DOCUMENTS_REFRESH_RATE = 1

# Global variable
logger = None
spark = None


class SimulationMessageSequenceDriver:

    mso_build_dir = None
    mso_build_uid = None
    learning_prop_config = None
    simulation_run_uid = None
    learning_home_dir = None

    def __init__(self, learning_home_dir, mso_build_uid):
        """
        :param learning_home_dir: path of learning home directory
        :param mso_build_uid: MSO build UID
        """
        assert os.path.isdir(learning_home_dir), "Learning home directory does not exists" + learning_home_dir

        self.learning_home_dir = learning_home_dir

        self.mso_build_uid = mso_build_uid

        print(self.learning_home_dir)

        # Create MSO build directory
        mso_build_dir = os.path.join(learning_home_dir, "builds", mso_build_uid)

        #print(mso_build_dir)

        # Validate MSO build directory
        assert self.__is_valid_mso_build_directory(mso_build_dir), "Invalid MSO Build Directory specified"

        # Set the MSO Build Directory
        self.mso_build_dir = mso_build_dir

        # Read learning.properties file
        self.__read_learning_properties()

        # Generate Simulation Build UID
        self.simulation_run_uid = uuid.uuid4()

        # Initialize logger
        self.__intialize_simulation_logger()

        # Initialize SparkDataAccessLayer
        initialize(self.__get_simuliation_logger_name(), spark)

        # Notify about the entry
        logger.info("Processing MSO Build Directory: " + mso_build_dir)

        # Write simulation run entry to LearningRun table
        self.__write_learning_run_entry()

    def __intialize_simulation_logger(self):
        """

        :return:
        """
        # Create simulation logger path
        timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
        simulation_log_file_path = self.learning_home_dir + "/../logs/simulation." + str(self.simulation_run_uid) + "." + timestamp + ".stdout"

        # Crete logger with file and output
        global logger
        logger_name = self.__get_simuliation_logger_name()
        logger = create_logger(logger_name, simulation_log_file_path, logging.DEBUG)

        # Initialize data access layer logger
        data_access_layer.initialize_logger(logger_name)

    def populate_email_data_from_archive(self):

        # Get the last updated value for the archive table
        learning_accessor = LearningAccessor()
        last_updated_time = learning_accessor.get_last_updated_for_sent_email_archive_table()

        if last_updated_time:
            last_updated_time = str(last_updated_time)
            last_updated_time = datetime.datetime.strptime(last_updated_time, SQL_TIMESTAMP_FORMAT)

            update_time_delta = datetime.datetime.now() - last_updated_time

            logger.debug("Sent Email Archive table updated previous days " + str(update_time_delta.days))

        """
        NOTE: Earlier simulation use to refreshes the data everyday. We are planning to move to using triggers for
        data loading. Hence copying from table only if `last_updated_time` is None i.e. No data exists in table.
        """
        if not last_updated_time:
            # Initialize the data in email table
            database_initializer = DatabaseIntializer()
            logger.info("Populating Sent Email Archive data  ..")
            database_initializer.populate_email_data_all_products()
            logger.info("Successfully loaded Sent Email Archive data.")
        else:
            logger.info("Sent Email Archive table updated today, Skipping the table preparation.")

    def populate_email_data_from_copy_storm(self):
        """
        This method copies `Approved_Document_vod__c` from copy storm database to `ApprovedDocuments` table in learning
        database.
        :return:
        """
        populate_flag = False
        if not os.path.exists("/tmp/nightly-simulation-lock.txt"):
            f = open('/tmp/nightly-simulation-lock.txt','a+')
            if os.stat('/tmp/nightly-simulation-lock.txt').st_size == 0:
                f.write('start populating...')
                populate_flag = True
            f.close()
        if populate_flag:
            database_initializer = DatabaseIntializer()
            database_initializer.populate_approved_documents_data()
            logger.info("Successfully loaded Approved Document data.")
            f = open('/tmp/nightly-simulation-lock.txt','w')
            f.write('finished')
            f.close()
            logger.info("Written status in tmp file")

    def __write_learning_run_entry(self):
        """
        This function writes simulation run entry in the `learningRun` table in learning schema.
        :return:
        """
        version_uid = self.learning_prop_config.get_property(LearningPropertiesKey.VERSION_UID)

        config_uid = self.learning_prop_config.get_property(LearningPropertiesKey.CONFIG_UID)

        # Write RunUID in the learningRun table
        learning_accessor = LearningAccessor()
        learning_accessor.write_simulation_run(self.simulation_run_uid, self.mso_build_uid, version_uid, config_uid)

        logger.info("Added entry in run database")

    def __update_learning_run_execution_status(self, status):
        """
        This fuction updates the status for current simulation run in `learningRun` table in learning schema.
        :param status: status value to update
        :return:
        """

        # Update simulation run execution status
        learning_accessor = LearningAccessor()
        learning_accessor.update_simulation_execution_status(self.simulation_run_uid, status)

    def __is_valid_mso_build_directory(self, mso_build_dir):
        """
        This function check if the specified directory is valid MSO Build directory.
        :return:
        """
        is_valid_mso_build_dir = True

        # Check if the directory exists
        if not os.path.isdir(mso_build_dir):
            is_valid_mso_build_dir = False
        else:
            # Check if there is learning properties file in Build directory
            learning_prop_file_path = os.path.join(mso_build_dir, LEARNING_PROPERTIES_FILE_NAME)
            if not os.path.isfile(learning_prop_file_path):
                is_valid_mso_build_dir = False
            pass

        return is_valid_mso_build_dir

    def __get_simuliation_logger_name(self):
        """
        This function returns the name of the simulation logger
        :return:
        """

        SIMULATION_LOGGER_NAME = "SIMULATION"
        sim_logger_name = SIMULATION_LOGGER_NAME + str(self.simulation_run_uid)
        return sim_logger_name

    def __read_learning_properties(self):
        """
        This function reads learning properties from the specified build directory.
        :return:
        """
        assert self.mso_build_dir

        learning_prop_file_path = os.path.join(self.mso_build_dir, LEARNING_PROPERTIES_FILE_NAME)

        assert os.path.isfile(learning_prop_file_path), "Learning Properties file does not exists"
        self.learning_prop_config = LearningPropertiesReader()
        self.learning_prop_config.read_learning_properties(learning_prop_file_path)
        # print(self.learning_prop_config.get_property("buildUID"))

    def generate_segments_v2(self):
        """
        This function creates segment by reading the learning properties file.
        :return:
        """

        # Read the segment values if from the learning properties
        segment_feature_names = self.learning_prop_config.get_property(LearningPropertiesKey.SEGMENTS)

        # If segment values is not None and empty
        if segment_feature_names and segment_feature_names != "":

            logger.info("Creating Segments for simulation...")

            # Split segment names into list separated by ';'
            segment_feature_names_1 = segment_feature_names.split(";")
            segment_feature_names = segment_feature_names.split(";")


            #remove extra string
            for i, s in enumerate(segment_feature_names_1):
                segment_feature_names_1[i] = s.replace("repAccountAttribute\\:","")
                segment_feature_names[i] = s.replace("repAccountAttribute\\:","")
                segment_feature_names_1[i] = s.replace("repAccountAttribute:","")
                segment_feature_names[i] = s.replace("repAccountAttribute:","")

            # Get Account data frame from the DSE
            logger.info("Fetching Account column name...")
            dse_accessor = DSEAccessor()
            account_col_names = dse_accessor.get_accounts_col_names()
            segment_feature_names_1 = list(set(segment_feature_names_1).intersection(set(account_col_names)))
            segment_feature_names = list(set(segment_feature_names).intersection(set(account_col_names)))
            logger.info("Fetching Account data frame...")
            accounts_df = dse_accessor.get_accounts_df(segment_feature_names_1)
            logger.info("Account data frame retrieved")

            segment_accessor = SegmentAccessor()

            if not accounts_df.empty:

                # Create an empty data frame for SimulationSegment table in learning database
                simulation_segment_df = pd.DataFrame(columns=['learningRunUID', 'learningBuildUID', 'segmentUID',
                                                              'segmentName', 'segmentValue', 'createdAt', 'updatedAt'])

                # Create a single data frame for each segment feature name (i.e. Account column name)
                master_segment_account_df = pd.DataFrame()

                for segment_feature_name in segment_feature_names:
                    logger.info("Generating segment for Feature Name " + segment_feature_name)

                    segment_feature_values = accounts_df[segment_feature_name].unique().tolist()

                    if len(segment_feature_values) > MAX_SEGMENT_FEATURE_VALUE_NUM:
                        logger.info("Too many unique values for " + segment_feature_name + ", skip this segment..")
                        continue

                    logger.debug("Unique feature values for segment variable= " + str(segment_feature_values))

                    # Iterate over all possible unique values for the account variable
                    for segment_feature_value in segment_feature_values:

                        logger.info("Generating segment for Feature Name '" + segment_feature_name + "' with Feature Value = " + str(segment_feature_value))

                        # Generate segment UID (NOTE: While writing to database it needs to be converted in string)
                        segment_uid = uuid.uuid4()

                        # Get time stamp to write to the database
                        ts = time.time()
                        timestamp = datetime.datetime.fromtimestamp(ts).strftime(SQL_TIMESTAMP_FORMAT)

                        # Add row to SimulationSegment data frame
                        simulation_segment_df = simulation_segment_df.append({'segmentUID': str(segment_uid), 'segmentName': segment_feature_name,
                                                      'segmentValue': segment_feature_value}, ignore_index=True)

                        # Filter account data frame based on segment values
                        simulation_account_segment_df = accounts_df.loc[(accounts_df[segment_feature_name] == segment_feature_value), ['externalId', 'accountName']]

                        # Add necessary values for accounts in the segment
                        simulation_account_segment_df['segmentUID'] = str(segment_uid)
                        simulation_account_segment_df['learningRunUID'] = str(self.simulation_run_uid)
                        simulation_account_segment_df['learningBuildUID'] = str(self.mso_build_uid)
                        simulation_account_segment_df['createdAt'] = timestamp
                        simulation_account_segment_df['updatedAt'] = timestamp

                        simulation_account_segment_df.rename(columns={'externalId':'accountUID'}, inplace=True)

                        # Append to a single data frame for each segment feature variable
                        master_segment_account_df = master_segment_account_df.append(simulation_account_segment_df)

                # Write SimulationSegmentAccount table to the database
                logger.info("Writing results to account segment table..")
                segment_accessor.write_simulation_account_segment_df(master_segment_account_df)
                logger.info("Finished writing results to account segment table")

                # Get timestamp for the Simulation Segment table
                ts = time.time()
                timestamp = datetime.datetime.fromtimestamp(ts).strftime(SQL_TIMESTAMP_FORMAT)

                # Add necessary values for the segment
                simulation_segment_df['learningRunUID'] = str(self.simulation_run_uid)
                simulation_segment_df['learningBuildUID'] = str(self.mso_build_uid)
                simulation_segment_df['createdAt'] = timestamp
                simulation_segment_df['updatedAt'] = timestamp

                # Write SimulationSegment table in learning database
                logger.info("Writing results to segment table..")
                segment_accessor.write_simulation_segment_df(simulation_segment_df)
                logger.info("Finished Writing results to segment table")

            else:
                logger.info("Account dataFrame is empty.")
        else:
            logger.info("No segment values provided.")

        logger.info("Finished creating segments.")

    def generate_segments(self):
        """
        This method will read the learning.properties file and generate required segments in the database
        :return:
        """
        # Read the segment values if from the learning properties
        segment_feature_names = self.learning_prop_config.get_property(LearningPropertiesKey.SEGMENTS)

        if segment_feature_names:

            logger.info("Creating Segments for simulation...")
            # Read the MSO Build UID
            mso_build_uid = self.learning_prop_config.get_property(LearningPropertiesKey.BUILD_UID)

            # If segment values is not None and empty
            if segment_feature_names and segment_feature_names != "":

                # Split segment names into list separated by ';'
                segment_feature_names = segment_feature_names.split(";")

                # Create segment accessor object
                segment_accessor = SegmentAccessor()

                # Iterate through segment features and create segments
                for segment_feature_name in segment_feature_names:
                    segment_accessor.create_account_segments(self.simulation_run_uid, mso_build_uid, segment_feature_name)

            logger.info("Generated Segments.")

        else:
            logger.info("No segment values provided.")

    def start_simulation(self):
        """
        This function will start the simulation for the initialized MSO Build
        :return:
        """

        try:

            # Generate Segments based on learning properties file
            segment_processor_thread = threading.Thread(target=self.generate_segments_v2, name='SimulationSegmentProcessor', daemon=False)
            segment_processor_thread.start()

            sim_logger_name = self.__get_simuliation_logger_name()

            # Initialize the message sequence predictor
            message_sequence_predictor = MessageSequencePredictor(self.simulation_run_uid, self.learning_prop_config,
                                                                   self.learning_home_dir, self.mso_build_uid, sim_logger_name, spark)

            # Calculate Scores for the sequences
            message_sequence_predictor.predict_for_sequences_v2()

            # TO-TEST::
            # Wait until segment processor thread has completed writing data
            if segment_processor_thread.is_alive():
                logger.info("Waiting for segment processing to complete..")
                segment_processor_thread.join()

            # Aggregate the scores for account segments
            # simulation_score_aggregator = SimulationScoreAggregator('e0ae4608-395e-4c38-96f4-131a626eb136', '8d66c8f9-c277-45c7-8269-eb4e865918b6', sim_logger_name)
            simulation_score_aggregator = SimulationScoreAggregator(self.simulation_run_uid, self.mso_build_uid, sim_logger_name)
            simulation_score_aggregator.calculate_segment_message_sequence()

            # Update simulation run execution status to success
            self.__update_learning_run_execution_status(SIMULATION_EXEC_SUCCESS_STATUS)

            # Clean up the simulation data
            self.perform_simulation_clean_up()

        except:

            logger.error("Error while running simulation.")

            # In case of exception write failure status for the simulation run
            self.__update_learning_run_execution_status(SIMULATION_EXEC_FAILURE_STATUS)

            # Raise the exception caught
            raise

    def perform_simulation_clean_up(self):
        """

        :return:
        """
        learning_accessor = LearningAccessor()

        learning_accessor.clean_up_simulation(self.mso_build_uid, self.simulation_run_uid)


    def load_simulation_data(self):
        """
        This function populates learning schema with required data for simulation process.
        :return:
        """
        logger.info("Data loading process started..")

        # Populate the email interaction data from the archive table
        logger.info("Loading account interaction data..")
        self.populate_email_data_from_archive()
        logger.info("Loading account interaction data complete!")

        # simulation_seq_driver.populate_email_data_from_copy_storm()
        logger.info("Loading email data from CS..")
        self.populate_email_data_from_copy_storm()
        logger.info("Loading email data from CS complete!")


def main():
    """
    Main entry point function to run the script takes configuration as input from the command line
    :return:
    """
    global spark

    # Validate the input argument to the script
    if len(sys.argv) != 11:
        logger.error("Invalid arguments to the simulation script")

    # Retrieve the arguments
    db_host = sys.argv[1]
    db_user = sys.argv[2]
    db_password = sys.argv[3]
    dse_db_name = sys.argv[4]
    cs_db_name = sys.argv[5]
    stage_db_name = sys.argv[6]
    learning_db_name = sys.argv[7]
    db_port = sys.argv[8]          # Update to 33066 for testing on Local machine
    learning_home_dir = sys.argv[9]
    mso_build_uid = sys.argv[10]

    # Create Learning database name

    # Create spark context
    spark = SparkSession.builder.appName("simulation" + mso_build_uid).getOrCreate()

    # Get the singleton instance of the database config and set the properties
    database_config = DatabaseConfig.instance()
    database_config.set_config(db_host, db_user, db_password, db_port, dse_db_name, learning_db_name, cs_db_name, stage_db_name)

    simulation_seq_driver = SimulationMessageSequenceDriver(learning_home_dir, mso_build_uid)

    # Populate the email interaction data from the archive table
    threading.Thread(target=simulation_seq_driver.load_simulation_data, name='SimulationDataLoader', daemon=False).start()

    simulation_seq_driver.start_simulation()


if __name__ == "__main__":
    main()
