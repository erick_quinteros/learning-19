FROM databricksruntime/standard:latest

# GETTING BUILD METADATA
ARG GIT_COMMIT
ARG GIT_URL
ARG GIT_BRANCH
ARG BUILD_NUMBER
ARG BUILD_URL

# ADDING BUILD METADATA AS ENV VARIABLES
ENV GIT_COMMIT=${GIT_COMMIT}
ENV GIT_URL=${GIT_URL}
ENV GIT_BRANCH=${GIT_BRANCH}
ENV BUILD_NUMBER=${BUILD_NUMBER}
ENV BUILD_URL=${BUILD_URL}

LABEL MAINTAINER="sundeep.nukaraju@aktana.com , sai.swaroop@aktana.com" \
      GIT_COMMIT="${GIT_COMMIT}" \
      GIT_URL="${GIT_URL}" \
      GIT_BRANCH="${GIT_BRANCH}" \
      BUILD_NUMBER="${BUILD_NUMBER}" \
      BUILD_URL="${BUILD_URL}"

ENV MLFLOW_TRACKING_URI=databricks
ENV DATABRICKS_HOST=https://aktana-poc.cloud.databricks.com
ENV DATABRICKS_TOKEN=dapidc5f8401da1715a24d5d592ce62c46ef

RUN /databricks/conda/envs/dcs-minimal/bin/pip install snowflake-connector-python==2.2.7

RUN mkdir /learning
COPY . /learning
