CREATE TABLE `SegmentTimeToEngageReport` (
  `learningRunUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `segment` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `segmentType` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `channelUID` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `intervalOrDoW` int(11) NOT NULL DEFAULT '1',
  `probability` double DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `method` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A',
  `tteAlgorithmName` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
);