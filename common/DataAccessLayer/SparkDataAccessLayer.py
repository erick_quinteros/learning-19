import configparser
import uuid
from common.DataAccessLayer.DatabaseConfig import DatabaseConfig
from common.DataAccessLayer.DataAccessLayer import MessageAccessor
from common.DataAccessLayer.DataAccessLayer import DSEAccessor
from common.DataAccessLayer.DataAccessLayer import LearningAccessor

import time
import datetime
import logging
import os
from pyspark.sql import SQLContext
from pyspark import SparkContext
from sqlalchemy import create_engine
import pandas as pd
from pysparkling import *
from pyspark.sql.functions import col, when

sqlContext = None
logger = None
PARTITION_COUNT = 64

account_cols = None
account_prod_cols = None

def initialize(logger_name, spark):
    """
    This function initializes the logger for the program
    :param simulation_logger_name:
    :return:
    """
    global logger
    global sqlContext

    logger = logging.getLogger(logger_name)

    sqlContext = SQLContext(spark)


class CommonDbAccessor:


    def write_pandas_dataframe(self, pandas_df, database, table_name, add_time_stamp = True, if_exists="append"):
        """

        :param pandas_df:
        :param database:
        :param table_name:
        :return:
        """
        # If add time stamp then add createdAt and updatedAt
        if add_time_stamp:
            # Add updated at and created at columns in the data frame
            ts = time.time()
            timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
            pandas_df['createdAt'] = timestamp
            pandas_df['updatedAt'] = timestamp

        # Get database config instance
        db_config = DatabaseConfig.instance()

        # Read the required MySQL configuration
        db_host = db_config.host
        db_user = db_config.user
        db_password = db_config.password
        db_name = database
        db_port = db_config.port

        # Generate Connection String
        connection_string = "mysql+pymysql://{user}:{pw}@{host}:{port}/{db}"
        connection_string = connection_string.format(user=db_user, pw=db_password, host=db_host,
                                                     port=db_port, db=db_name)
        # Create engine
        engine = create_engine(connection_string, pool_size=10, max_overflow=20)

        pandas_df.to_sql(con=engine, name=table_name, if_exists=if_exists, index=False, chunksize=10000)


class SparkDSEAccessor:
    """
    This class is responsible for reading data from DSE database returning results in spark compatible formats.
    """

    def get_account_id_external_id_df(self):
        """

        :return:

        """
        global account_cols

        dbconfig = DatabaseConfig.instance()
        hostname = dbconfig.host
        dbname = dbconfig.dse_db_name
        jdbcPort = dbconfig.port
        username = dbconfig.user
        password = dbconfig.password

        jdbc_url = "jdbc:mysql://{0}:{1}/{2}?user={3}&password={4}".format(hostname, jdbcPort, dbname, username,
                                                                           password)
        table_name = "(SELECT accountId, externalId FROM {DB_NAME}.Account) as dbtable".format(DB_NAME=dbname)

        account_df = sqlContext.read.format('jdbc').options(driver='com.mysql.jdbc.Driver', url=jdbc_url,
                                                            dbtable=table_name,
                                                            numPartitions=PARTITION_COUNT).load()

        return account_df

    def get_rep_df(self):
        """
        This function returns the rep dataframe.
        """
        global rep_cols

        dbconfig = DatabaseConfig.instance()
        hostname = dbconfig.host
        dbname = dbconfig.dse_db_name
        jdbcPort = dbconfig.port
        username = dbconfig.user
        password = dbconfig.password

        jdbc_url = "jdbc:mysql://{0}:{1}/{2}?user={3}&password={4}".format(hostname, jdbcPort, dbname, username,
                                                                           password)
        table_name = "(SELECT * FROM {DB_NAME}.Rep) as dbtable".format(DB_NAME=dbname)

        rep_df = sqlContext.read.format('jdbc').options(driver='com.mysql.jdbc.Driver', url=jdbc_url,
                                                            dbtable=table_name,
                                                            numPartitions=PARTITION_COUNT).load()

        return rep_df

    def get_run_df(self, published_suggestions_condition):
        """
        This function returns the run dataframe.
        """
        global run_cols

        dbconfig = DatabaseConfig.instance()
        hostname = dbconfig.host
        dbname = dbconfig.dse_db_name
        jdbcPort = dbconfig.port
        username = dbconfig.user
        password = dbconfig.password

        jdbc_url = "jdbc:mysql://{0}:{1}/{2}?user={3}&password={4}".format(hostname, jdbcPort, dbname, username,
                                                                           password)
        table_name = "(SELECT * FROM {DB_NAME}.DSERun DR {pub_sugg_cond}) as dbtable".format(DB_NAME=dbname, pub_sugg_cond=published_suggestions_condition)


        run_df = sqlContext.read.format('jdbc').options(driver='com.mysql.jdbc.Driver', url=jdbc_url,
                                                            dbtable=table_name,
                                                            numPartitions=PARTITION_COUNT).load()

        return run_df

    def get_run_rep_date_df(self):
        """
        This function returns the run rep date dataframe.
        """
        global run_rep_date_cols

        dbconfig = DatabaseConfig.instance()
        hostname = dbconfig.host
        dbname = dbconfig.dse_db_name
        jdbcPort = dbconfig.port
        username = dbconfig.user
        password = dbconfig.password

        jdbc_url = "jdbc:mysql://{0}:{1}/{2}?user={3}&password={4}".format(hostname, jdbcPort, dbname, username,
                                                                           password)
        table_name = "(SELECT * FROM {DB_NAME}.DSERunRepDate) as dbtable".format(DB_NAME=dbname)


        run_rep_date_df = sqlContext.read.format('jdbc').options(driver='com.mysql.jdbc.Driver', url=jdbc_url,
                                                            dbtable=table_name,
                                                            numPartitions=PARTITION_COUNT).load()

        return run_rep_date_df

    def get_spark_run_df(self, published_suggestions_condition):
        """
        This function returns the spark run dataframe.
        """
        global spark_run_cols

        dbconfig = DatabaseConfig.instance()
        hostname = dbconfig.host
        dbname = dbconfig.dse_db_name
        jdbcPort = dbconfig.port
        username = dbconfig.user
        password = dbconfig.password

        jdbc_url = "jdbc:mysql://{0}:{1}/{2}?user={3}&password={4}".format(hostname, jdbcPort, dbname, username,
                                                                           password)
        table_name = "(SELECT * FROM {DB_NAME}.SparkDSERun DR {pub_sugg_cond}) as dbtable".format(DB_NAME=dbname, pub_sugg_cond=published_suggestions_condition)


        spark_run_df = sqlContext.read.format('jdbc').options(driver='com.mysql.jdbc.Driver', url=jdbc_url,
                                                            dbtable=table_name,
                                                            numPartitions=PARTITION_COUNT).load()

        return spark_run_df

    def get_spark_run_rep_date_df(self):
        """
        This function returns the spark run rep date dataframe.
        """
        global spark_run_rep_date_cols

        dbconfig = DatabaseConfig.instance()
        hostname = dbconfig.host
        dbname = dbconfig.dse_db_name
        jdbcPort = dbconfig.port
        username = dbconfig.user
        password = dbconfig.password

        jdbc_url = "jdbc:mysql://{0}:{1}/{2}?user={3}&password={4}".format(hostname, jdbcPort, dbname, username,
                                                                           password)
        table_name = "(SELECT * FROM {DB_NAME}.SparkDSERunRepDate) as dbtable".format(DB_NAME=dbname)


        spark_run_rep_date_df = sqlContext.read.format('jdbc').options(driver='com.mysql.jdbc.Driver', url=jdbc_url,
                                                            dbtable=table_name,
                                                            numPartitions=PARTITION_COUNT).load()

        return spark_run_rep_date_df


class SparkLearningAccessor:
    """
    This function is responsible for spark learning database access.
    """

    def drop_duplicate_col_account(self, account_df):
        global account_cols
        global account_prod_cols
        dup_cols = list((set(account_df.columns) & set(account_prod_cols))-{"accountId"})
        if dup_cols:
            account_df = account_df.drop(*dup_cols)
        account_cols = [c for c in account_df.columns if c not in dup_cols]
        return account_df

    def get_account_table(self):
        """
        This function returns spark dataframe as all account table.
        :return:
        """
        global account_cols

        dbconfig = DatabaseConfig.instance()
        hostname = dbconfig.host
        dbname = dbconfig.dse_db_name
        jdbcPort = dbconfig.port
        username = dbconfig.user
        password = dbconfig.password


        jdbc_url = "jdbc:mysql://{0}:{1}/{2}?user={3}&password={4}".format(hostname, jdbcPort, dbname, username,
                                                                           password)
        table_name = "(SELECT * FROM {DB_NAME}.Account) as dbtable".format(DB_NAME=dbname)

        account_df = sqlContext.read.format('jdbc').options(driver='com.mysql.jdbc.Driver', url=jdbc_url, dbtable=table_name,
                                                            numPartitions=PARTITION_COUNT).load()

        account_cols = account_df.columns

        return account_df

    def get_account_product_table(self, product_uid):
        """
        This function returns the account product table subset for the specified product.
        :param product_uid:
        :return:
        """
        global account_prod_cols

        dbconfig = DatabaseConfig.instance()
        hostname = dbconfig.host
        dbname = dbconfig.dse_db_name
        jdbcPort = dbconfig.port
        username = dbconfig.user
        password = dbconfig.password
        jdbc_url = "jdbc:mysql://{0}:{1}/{2}?user={3}&password={4}".format(hostname, jdbcPort, dbname, username,
                                                                           password)
        message_accessor = MessageAccessor()
        product_id = message_accessor.get_product_id_for_uid(product_uid)
        query = "(SELECT * FROM {DB_NAME}.AccountProduct WHERE productId={PRODUCT_ID}) AS dbtable".format(DB_NAME=dbname, PRODUCT_ID=product_id)

        account_product_df = sqlContext.read.format('jdbc').options(driver='com.mysql.jdbc.Driver', url=jdbc_url,
                                                                    dbtable=query,
                                                                    numPartitions=PARTITION_COUNT).load()

        account_prod_cols = account_product_df.columns

        return account_product_df

    def get_account_product_table_all(self):
        """
        This function returns the account product table subset for the specified product.
        :param product_uid:
        :return:
        """
        global account_prod_cols

        dbconfig = DatabaseConfig.instance()
        hostname = dbconfig.host
        dbname = dbconfig.dse_db_name
        jdbcPort = dbconfig.port
        username = dbconfig.user
        password = dbconfig.password
        jdbc_url = "jdbc:mysql://{0}:{1}/{2}?user={3}&password={4}".format(hostname, jdbcPort, dbname, username,
                                                                           password)
        
        learning_accessor = LearningAccessor()
        product_uids = learning_accessor.get_enabled_products_for_learning()
        message_accessor = MessageAccessor()
        product_ids = []
        for product_uid in product_uids:
            product_id = message_accessor.get_product_id_for_uid(product_uid)
            product_ids.append(str(product_id))
        product_ids_str = ",".join(product_ids)
        query = "(SELECT * FROM {DB_NAME}.AccountProduct WHERE productId in ({PRODUCT_IDS_STR})) AS dbtable".format(DB_NAME=dbname, PRODUCT_IDS_STR=product_ids_str)

        account_product_df = sqlContext.read.format('jdbc').options(driver='com.mysql.jdbc.Driver', url=jdbc_url,
                                                                    dbtable=query,
                                                                    numPartitions=64, partitionColumn="accountId", lowerBound=1000, upperBound=185444083).load()

        account_prod_cols = account_product_df.columns

        return account_product_df
    

    def get_target_column(self, product_uid, goal):
        """

        :param product_uid:
        :return:
        """
        dbconfig = DatabaseConfig.instance()

        hostname = dbconfig.host
        dbname = dbconfig.dse_db_name
        jdbcPort = dbconfig.port
        username = dbconfig.user
        password = dbconfig.password
        jdbc_url = "jdbc:mysql://{0}:{1}/{2}?user={3}&password={4}".format(hostname, jdbcPort, dbname, username,
                                                                           password)

        message_accessor = MessageAccessor()
        dse_accessor = DSEAccessor()
        product_id = message_accessor.get_product_id_for_uid(product_uid)
        event_type_id = dse_accessor.get_event_type_id_for_goal(goal)
        query = "(SELECT accountId, 1 as 'TARGET' FROM {DB_NAME}.Event WHERE productId={PRODUCT_ID} " \
                "AND eventTypeId='{EVENT_TYPE_ID}') AS dbtable".format(DB_NAME=dbname,
                                                                                PRODUCT_ID=product_id,
                                                                                EVENT_TYPE_ID=event_type_id)

        account_target_df = sqlContext.read.format('jdbc').options(driver='com.mysql.jdbc.Driver', url=jdbc_url,
                                                                    dbtable=query,
                                                                    numPartitions=PARTITION_COUNT).load()

        return account_target_df


    def get_account_event_table(self):
        """
        This function returns the account product table subset for the specified product.
        :param product_uid:
        :return:
        """
        # global account_prod_cols

        dbconfig = DatabaseConfig.instance()
        hostname = dbconfig.host
        dbname = dbconfig.dse_db_name
        jdbcPort = dbconfig.port
        username = dbconfig.user
        password = dbconfig.password
        jdbc_url = "jdbc:mysql://{0}:{1}/{2}?user={3}&password={4}".format(hostname, jdbcPort, dbname, username,
                                                                           password)
        # message_accessor = MessageAccessor()
        # product_id = message_accessor.get_product_id_for_uid(product_uid)
        query = "(SELECT  e.repId, e.accountId, e.eventDate, et.eventTypeName, e.eventLabel, e.eventDateTimeUTC, e.messageId, e.physicalMessageUID, e.productId FROM {DB_NAME}.Event as e JOIN {DB_NAME}.EventType et ON e.eventTypeId=et.eventTypeId) AS dbtable".format(DB_NAME=dbname)

        account_event_df = sqlContext.read.format('jdbc').options(driver='com.mysql.jdbc.Driver', url=jdbc_url,
                                                                    dbtable=query,
                                                                    numPartitions=PARTITION_COUNT).load()

        account_event_df = account_event_df.toPandas()
        account_event_df = account_event_df.rename(columns={"eventDate":"date", "eventTypeName":"type"})
        account_event_df['date'] = pd.to_datetime(account_event_df['date'])
        return account_event_df[['accountId','type','date']]

    def get_account_interaction_table(self):
        """
        This function returns the account product table subset for the specified product.
        :param product_uid:
        :return:
        """
        # global account_prod_cols

        dbconfig = DatabaseConfig.instance()
        hostname = dbconfig.host
        dbname = dbconfig.dse_db_name
        jdbcPort = dbconfig.port
        username = dbconfig.user
        password = dbconfig.password
        jdbc_url = "jdbc:mysql://{0}:{1}/{2}?user={3}&password={4}".format(hostname, jdbcPort, dbname, username,
                                                                           password)
        # message_accessor = MessageAccessor()
        # product_id = message_accessor.get_product_id_for_uid(product_uid)
        query = "(SELECT  v.interactionId, v.externalId, v.repId, v.facilityId, v.wasCreatedFromSuggestion, ia.accountId, IFNULL(ipm.messageId, vp.messageId) as messageId, IFNULL(ipm.physicalMessageUID, vp.physicalMessageUID) as physicalMessageUID, vp.productId, vp.productInteractionTypeId,pit.`productInteractionTypeName`, v.duration, v.startDateLocal FROM {DB_NAME}.Interaction as v JOIN {DB_NAME}.InteractionProduct vp ON v.isCompleted=1 AND v.isDeleted=0 AND v.interactionId=vp.interactionId JOIN {DB_NAME}.InteractionAccount ia ON v.interactionId=ia.interactionId LEFT JOIN {DB_NAME}.InteractionProductMessage ipm ON vp.`interactionId` = ipm.`interactionId` AND vp.`productId` = ipm.`productId` JOIN {DB_NAME}.ProductInteractionType pit on vp.productInteractionTypeId = pit.`productInteractionTypeId` where v.startDateLocal>='2019-04-01') AS dbtable".format(DB_NAME=dbname)

        account_interaction_df = sqlContext.read.format('jdbc').options(driver='com.mysql.jdbc.Driver', url=jdbc_url,
                                                                    dbtable=query,
                                                                    numPartitions=PARTITION_COUNT,
                                                                    partitionColumn='repId',
                                                                    lowerBound=1000, upperBound=135500000).load()

        account_interaction_df = account_interaction_df.toPandas()
        print("Got Interaction DF")
        dse_accessor = DSEAccessor()
        # productInteractionType_df = dse_accessor.get_product_interaction_type_df()
        # account_interaction_df = account_interaction_df.join(productInteractionType_df, on="productInteractionTypeId",  how='left')
        # account_interaction_df = account_interaction_df.drop(columns=["productInteractionTypeId","productId"])
        account_interaction_df = account_interaction_df.rename(columns={"startDateLocal":"date", "productInteractionTypeName":"type"})
        account_interaction_df['date'] = pd.to_datetime(account_interaction_df['date'])
        print("Get call2sampleId_df...")
        call2sampleId_df = dse_accessor.get_call2sampleId_df()
        account_interaction_df = account_interaction_df[~account_interaction_df['externalId'].isin(call2sampleId_df['Id'])]
        print("Finish interaction df")
        return account_interaction_df[['accountId','type','date']]

    def write_variable_importance(self, variable_importance_df, goal, channel, product_uid):
        """

        :param variable_importance:
        :return:
        """

        variable_importance_df = variable_importance_df.drop(columns=['relative_importance', 'percentage'])
        variable_importance_df.rename(columns={'variable': 'predictor', 'scaled_importance': 'probability'}, inplace=True)
        variable_importance_df['productUID'] = product_uid
        variable_importance_df['channelUID'] = channel
        variable_importance_df['goal'] = goal

        # Update the source of the predictor based on Account or AccountProduct table
        variable_importance_df.loc[variable_importance_df['predictor'].isin(account_cols),'source'] = "ACCOUNT"
        variable_importance_df.loc[variable_importance_df['predictor'].isin(account_prod_cols), 'source'] = "ACCOUNT_PRODUCT"

        db_config = DatabaseConfig.instance()
        learning_db_name = db_config.learning_db_name

        common_db_accessor = CommonDbAccessor()
        common_db_accessor.write_pandas_dataframe(variable_importance_df, learning_db_name,
                                                  "MessageSequencePredictorInfo")

    def write_variable_importance_tte(self, variable_importance_df, goal, channel, product_uid):
        """

        :param variable_importance:
        :return:
        """

        variable_importance_df = variable_importance_df.drop(columns=['relative_importance', 'percentage'])
        variable_importance_df.rename(columns={'variable': 'predictor', 'scaled_importance': 'probability'}, inplace=True)
        # variable_importance_df['productUID'] = product_uid
        variable_importance_df['channelUID'] = channel
        variable_importance_df['goal'] = goal

        # Update the source of the predictor based on Account or AccountProduct table
        variable_importance_df.loc[variable_importance_df['predictor'].isin(account_cols),'source'] = "ACCOUNT"
        variable_importance_df.loc[variable_importance_df['predictor'].isin(account_prod_cols), 'source'] = "ACCOUNT_PRODUCT"

        db_config = DatabaseConfig.instance()
        learning_db_name = db_config.learning_db_name

        common_db_accessor = CommonDbAccessor()
        common_db_accessor.write_pandas_dataframe(variable_importance_df, learning_db_name,
                                                  "MessageTimingPredictorInfo")

    def write_optimal_learning_params(self, top_predictor_str, goal, channel, product_uid):
        """

        :param top_predictor_str:
        :param goal:
        :param channel:
        :param product_uid:
        :return:
        """
        data = {'paramName':['LE_MS_addPredictorsFromAccountProduct', 'LE_MS_includeVisitChannel',
                             'LE_MS_removeMessageClicks', 'LE_MS_removeMessageOpens', 'LE_MS_removeMessageSends'],
                'paramValue':[top_predictor_str, '1','0','0','0']}


        optimal_params_df = pd.DataFrame(data=data)
        optimal_params_df["channelUID"] = channel
        optimal_params_df["goal"] = goal
        optimal_params_df["productUID"] = product_uid

        db_config = DatabaseConfig.instance()
        learning_db_name = db_config.learning_db_name

        common_db_accessor = CommonDbAccessor()
        common_db_accessor.write_pandas_dataframe(optimal_params_df, learning_db_name,
                                                  "OptimalLearningParams")

    def write_optimal_learning_params_tte(self, top_predictor_str, goal, channel, product_uid):
        """

        :param top_predictor_str:
        :param goal:
        :param channel:
        :param product_uid:
        :return:
        """
        data = {'paramName':['LE_MT_addPredictorsFromAccountProduct', 'LE_MT_includeEventChannel', 'LE_MT_includeSendChannel', 'LE_MT_includeVisitChannel'],
                'paramValue':[top_predictor_str, '1', '1', '1']}


        optimal_params_df = pd.DataFrame(data=data)
        optimal_params_df["channelUID"] = channel
        optimal_params_df["goal"] = goal
        optimal_params_df["productUID"] = product_uid

        db_config = DatabaseConfig.instance()
        learning_db_name = db_config.learning_db_name

        common_db_accessor = CommonDbAccessor()
        common_db_accessor.write_pandas_dataframe(optimal_params_df, learning_db_name,
                                                  "OptimalLearningParams")

    def get_rep_engagement_table(self):
        """
        This function returns the rep engagement table.
        :return:
        """

        dbconfig = DatabaseConfig.instance()
        hostname = dbconfig.host
        dbname = dbconfig.learning_db_name
        jdbcPort = dbconfig.port
        username = dbconfig.user
        password = dbconfig.password


        jdbc_url = "jdbc:mysql://{0}:{1}/{2}?user={3}&password={4}".format(hostname, jdbcPort, dbname, username,
                                                                           password)
        table_name = "(SELECT * FROM {DB_NAME}.RepEngagementCalculation) as dbtable".format(DB_NAME=dbname)

        rep_engagement_df = sqlContext.read.format('jdbc').options(driver='com.mysql.jdbc.Driver', url=jdbc_url, dbtable=table_name,
                                                            numPartitions=PARTITION_COUNT).load()

        return rep_engagement_df

    def write_rep_engagement_table(self, df):
        """
        This function writes to the Rep Engagement Calculation table.
        :return:
        """

        dbconfig = DatabaseConfig.instance()
        hostname = dbconfig.host
        dbname = dbconfig.learning_db_name
        jdbcPort = dbconfig.port
        username = dbconfig.user
        password = dbconfig.password


        jdbc_url = "jdbc:mysql://{0}:{1}/{2}?user={3}&password={4}".format(hostname, jdbcPort, dbname, username,
                                                                           password)
        df.write.format('jdbc').options(url=jdbc_url, driver='com.mysql.jdbc.Driver',
                                              dbtable='RepEngagementCalculation',
                                              user=username, password=password).mode('append').save()


class SparkCSAccessor:
    """
    This function is responsible for spark cs database access.
    """

    def get_sync_info_table(self, synched_Suggestions_condition):
        """
        This function returns the sync info table.
        :return:
        """
        global sync_info_cols

        dbconfig = DatabaseConfig.instance()
        hostname = dbconfig.host
        dbname = dbconfig.cs_db_name
        jdbcPort = dbconfig.port
        username = dbconfig.user
        password = dbconfig.password


        jdbc_url = "jdbc:mysql://{0}:{1}/{2}?user={3}&password={4}".format(hostname, jdbcPort, dbname, username,
                                                                           password)
        table_name = "(SELECT * FROM {DB_NAME}.Sync_Tracking_vod__c A WHERE A.Successful_Sync_vod__c = 1 AND A.Canceled_vod__c = 0 {sync_sugg_cond}) as dbTable".format(DB_NAME=dbname, sync_sugg_cond=synched_Suggestions_condition)

        sync_info_df = sqlContext.read.format('jdbc').options(driver='com.mysql.jdbc.Driver', url=jdbc_url, dbtable=table_name,
                                                            numPartitions=PARTITION_COUNT).load()
        sync_info_cols = sync_info_df.columns

        return sync_info_df


class SparkStageAccessor:
    """
    This function is responsible for spark stage database access.
    """

    def get_AKT_rep_license_arc(self):
        """
        This function returns AKT Rep License Arc.
        :return:
        """

        dbconfig = DatabaseConfig.instance()
        hostname = dbconfig.host
        dbname = dbconfig.stage_db_name
        jdbcPort = dbconfig.port
        username = dbconfig.user
        password = dbconfig.password


        jdbc_url = "jdbc:mysql://{0}:{1}/{2}?user={3}&password={4}".format(hostname, jdbcPort, dbname, username,
                                                                           password)
        table_name = "(SELECT * FROM {DB_NAME}.AKT_RepLicense_arc) as dbtable".format(DB_NAME=dbname)

        AKT_rep_license_arc = sqlContext.read.format('jdbc').options(driver='com.mysql.jdbc.Driver', url=jdbc_url, dbtable=table_name,
                                                            numPartitions=PARTITION_COUNT).load()

        return AKT_rep_license_arc

    def get_rpt_dim_calendar(self):
        """
        This function returns rpt dim Calendar.
        :return:
        """

        dbconfig = DatabaseConfig.instance()
        hostname = dbconfig.host
        dbname = dbconfig.stage_db_name
        jdbcPort = dbconfig.port
        username = dbconfig.user
        password = dbconfig.password


        jdbc_url = "jdbc:mysql://{0}:{1}/{2}?user={3}&password={4}".format(hostname, jdbcPort, dbname, username,
                                                                           password)
        table_name = "(SELECT * FROM {DB_NAME}.rpt_dim_calendar) as dbtable".format(DB_NAME=dbname)

        rpt_dim_calendar = sqlContext.read.format('jdbc').options(driver='com.mysql.jdbc.Driver', url=jdbc_url, dbtable=table_name,
                                                            numPartitions=PARTITION_COUNT).load()

        return rpt_dim_calendar

    def get_rpt_Suggestion_Delivered_stg(self, start, end):
        """
        This function returns rpt Suggestions Delivered stg.
        :return:
        """

        dbconfig = DatabaseConfig.instance()
        hostname = dbconfig.host
        dbname = dbconfig.stage_db_name
        jdbcPort = dbconfig.port
        username = dbconfig.user
        password = dbconfig.password


        jdbc_url = "jdbc:mysql://{0}:{1}/{2}?user={3}&password={4}".format(hostname, jdbcPort, dbname, username,
                                                                           password)
        table_name = "(SELECT * FROM {DB_NAME}.RPT_Suggestion_Delivered_stg{START}{END} group by suggestionReferenceId) as dbtable".format(DB_NAME=dbname, START=start, END=end)

        rpt_suggestions_delivered_df = sqlContext.read.format('jdbc').options(driver='com.mysql.jdbc.Driver', url=jdbc_url, dbtable=table_name,
                                                            numPartitions=PARTITION_COUNT).load()

        return rpt_suggestions_delivered_df
