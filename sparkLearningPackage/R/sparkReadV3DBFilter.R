##########################################################
#
#
# aktana-learning Install Aktana Learning Engines.
#
# description: read the DB 
#
#
# created by : shirley.xu@aktana.com
#
# created on : 2019-04-0
#
# Copyright AKTANA (c) 2019.
#
#
##########################################################

sparkReadV3db.products <- function(sc, sparkDBconURL, con, productUID) {
  flog.info("read products")
  if (productUID!="All") {
    products <- readV3db.products(con, productUID)
  } else {
    productsSQL <- "SELECT productName, externalId, productId FROM Product WHERE isActive=1 and isDeleted=0"
    products <- sparkReadFromDB(sc, sparkDBconURL, productsSQL, name="products", memory=TRUE, repartition=1)
  }
  return(products)
}

sparkReadV3db.accounts <- function(sc, sparkDBconURL, accts) {
  flog.info("read accounts")
  accounts <- sparkReadFromDB(sc, sparkDBconURL, "SELECT * FROM Account WHERE isDeleted=0", name="accounts")
  accounts <- accounts %>% filter(accountId %in% accts)
  return(accounts)
}

sparkReadV3db.accountProduct <- function(sc, sparkDBconURL, con, accts, products, prods=NULL) {
  # check whether products is filtered
  definedProduct <- !is.na(nrow(products)) & nrow(products)==1
  
  if (definedProduct) { # productUID != 'All'
    flog.info("read accountProduct: %s",products$productName)
    accountProductSQL <- sprintf("SELECT * FROM AccountProduct where productId=%s", products$productId)
  } else {
    flog.info("read accountProduct: All")
    
    acctProd <- data.table(dbGetQuery(con, "select * from AccountProduct limit 1"))
    acctProd.names <- names(acctProd)
    
    # get only column names in AccountProduct table
    if (length(prods) > 0) {
      prods <- prods[prods %in% acctProd.names]
    }
    
    if(prods == 0 || length(prods) == 0 ) {
      accountProductSQL <- "SELECT accountId, productId FROM AccountProduct"
    } else {
      accountProductSQL <- sprintf("SELECT accountId, productId, %s FROM AccountProduct", paste(unlist(prods), collapse=","))
    }
    
    # flog.info("accountProductSQL=%s", accountProductSQL)
  }
  accountProduct <- sparkReadFromDB(sc, sparkDBconURL, accountProductSQL, name="accountProduct")
  # filter by accts
  accountProduct <- accountProduct %>% filter(accountId %in% accts)
  # add productName
  if (definedProduct) {
    prodName <- products$productName
    accountProduct <- accountProduct %>% mutate(productName=prodName) %>% select(-productId)
  } else {
    accountProduct <- accountProduct %>% 
      inner_join(select(products,c("productId","productName")), by="productId") %>%
      select(-productId)
  }
  return(accountProduct)
}

sparkReadV3db.interactions <- function(sc, sparkDBconURL, accts, products) {
  # check whether products is filtered
  definedProduct <- !is.na(nrow(products)) & nrow(products)==1
  
  if (definedProduct) { # productUID != 'All'
    flog.info("read interactions: %s",products$productName)
    interactionsSQL <- sprintf("SELECT  v.interactionId, v.externalId, v.repId, v.facilityId, v.wasCreatedFromSuggestion, ia.accountId, IFNULL(ipm.messageId, vp.messageId) as messageId, IFNULL(ipm.physicalMessageUID, vp.physicalMessageUID) as physicalMessageUID, vp.productId, v.duration, v.startDateLocal, pit.productInteractionTypeName FROM Interaction as v JOIN InteractionProduct vp ON v.isCompleted=1 AND v.isDeleted=0 AND vp.productId=%s AND v.interactionId=vp.interactionId JOIN InteractionAccount ia ON v.interactionId=ia.interactionId LEFT JOIN InteractionProductMessage ipm ON vp.`interactionId` = ipm.`interactionId` AND vp.`productId` = ipm.`productId` JOIN ProductInteractionType pit ON vp.productInteractionTypeId=pit.productInteractionTypeId", products$productId)
  } else {
    flog.info("read interactions: All")
    interactionsSQL <- "SELECT  v.interactionId, v.externalId, v.repId, v.facilityId, v.wasCreatedFromSuggestion, ia.accountId, IFNULL(ipm.messageId, vp.messageId) as messageId, IFNULL(ipm.physicalMessageUID, vp.physicalMessageUID) as physicalMessageUID, vp.productId, v.duration, v.startDateLocal, productInteractionTypeName FROM Interaction as v JOIN InteractionProduct vp ON v.isCompleted=1 AND v.isDeleted=0 AND v.interactionId=vp.interactionId JOIN InteractionAccount ia ON v.interactionId=ia.interactionId LEFT JOIN InteractionProductMessage ipm ON vp.`interactionId` = ipm.`interactionId` AND vp.`productId` = ipm.`productId` JOIN ProductInteractionType pit ON vp.productInteractionTypeId=pit.productInteractionTypeId"
  }
  interactions <- sparkReadFromDB(sc, sparkDBconURL, interactionsSQL, name="interactions")
  # filter by accts
  interactions <- interactions %>% filter(accountId %in% accts)
  # change date data type
  interactions <- interactions %>% 
    dplyr::rename(date=startDateLocal)
  # add productName
  if (definedProduct) {
    prodName <- products$productName
    interactions <- interactions %>% mutate(productName=prodName) %>% select(-productId)
  } else {
    interactions <- interactions %>% 
      inner_join(select(products,c("productId","productName")), by="productId") %>%
      select(-productId)
  }
  return(interactions)
}

sparkReadV3db.events <- function(sc, sparkDBconURL, accts, products) {
  # check whether products is filtered
  definedProduct <- !is.na(nrow(products)) & nrow(products)==1
  
  if (definedProduct) { # productUID != 'All'
    flog.info("read events: %s",products$productName)
    eventsSQL <- sprintf("SELECT  e.repId, e.accountId, e.eventDate, et.eventTypeName, e.eventLabel, e.eventDateTimeUTC, e.messageId, e.physicalMessageUID, e.productId FROM .Event as e JOIN .EventType et ON e.productId=%s AND e.eventTypeId=et.eventTypeId", products$productId)
  } else {
    flog.info("read events: All")
    eventsSQL <- "SELECT  e.repId, e.accountId, e.eventDate, et.eventTypeName, e.eventLabel, e.eventDateTimeUTC, e.messageId, e.physicalMessageUID, e.productId FROM .Event as e JOIN .EventType et ON e.eventTypeId=et.eventTypeId"
  }
  events <- sparkReadFromDB(sc, sparkDBconURL, eventsSQL, name="events")
  # process 
  events <- events %>% filter(accountId %in% accts)
  # add productName
  if (definedProduct) {
    prodName <- products$productName
    events <- events %>% mutate(productName=prodName) %>% select(-productId)
  } else {
    events <- events %>% 
      inner_join(select(products,c("productId","productName")), by="productId") %>%    # add prodcutName
      select(-productId)
  }
  return(events)
}

sparkReadV3db.processProducts <- function(products) {
  # check whether products is filtered
  definedProduct <- !is.na(nrow(products)) & nrow(products)==1
  
  if (definedProduct) {
    products <- products[,c("productName","externalId")]
  } else {
    products <- data.frame(collect(products))[,c("productName","externalId")]
  }
}


##########################################################################################################
#                                               Main Function
###########################################################################################################

sparkReadV3dbFilter <- function(sc, sparkDBconURL, con, productUID, readDataList=NULL, prods=NULL)
{
  if (is.null(readDataList)) {
    readDataList <- c("accounts","interactions","messages","messageSetMessage","messageSet","accountProduct","events","products")
    flog.info("read V3db data: all (%s)", paste(readDataList,collapse=","))
  } else {
    flog.info("read V3db data: %s", paste(readDataList,collapse=","))
  }
  # initialize loading
  loadedData <- list()
  dbGetQuery(con,"SET NAMES utf8;")
  
  # table loading not affected by the input productUID and accts filtering
  if ("messages" %in% readDataList) {
    loadedData[["messages"]] <- readV3db.messages(con)
  }
  
  if ("messageSetMessage" %in% readDataList) {
    loadedData[["messageSetMessage"]] <- readV3db.messageSetMessage(con)
  }
  
  if ("messageSet" %in% readDataList) {
    loadedData[["messageSet"]] <- readV3db.messageSet(con)
  }
  
  # table loading needs accts filter
  if (any(readDataList %in% c("accounts","interactions","accountProduct","events"))) {
    accts <- readV3db.getFilterAcctsList(con)
  }
  
  if ("accounts" %in% readDataList) {
    loadedData[["accounts"]] <- sparkReadV3db.accounts(sc,sparkDBconURL,accts)
  }
  
  # table loading affected by the input productUID
  if (any(readDataList %in% c("interactions","accountProduct","events"))) {
    products <- sparkReadV3db.products(sc, sparkDBconURL, con, productUID)
  }
  
  if ("accountProduct" %in% readDataList) {
    loadedData[["accountProduct"]] <- sparkReadV3db.accountProduct(sc,sparkDBconURL,con,accts,products,prods)
  }
  
  if ("interactions" %in% readDataList) {
    loadedData[["interactions"]] <- sparkReadV3db.interactions(sc,sparkDBconURL,accts,products)
  }
  
  if ("events" %in% readDataList) {
    loadedData[["events"]] <- sparkReadV3db.events(sc,sparkDBconURL,accts,products)
  }
  
  if ("products" %in% readDataList) {
    loadedData[["products"]] <- sparkReadV3db.processProducts(products)
  }
  
  flog.info("return from V3readFilter")
  return(loadedData)
}