context('testing saveAccountDateLocation, saveAccountDateLocationScores in ANCHOR module')
print(Sys.time())

# load library and source script
library(data.table)
library(Learning.DataAccessLayer)
source(sprintf("%s/anchor/code/utils.r",homedir))
source(sprintf("%s/anchor/code/saveAccountDateLocationResult.r",homedir))

# set up DB connection required for function
dataAccessLayer.common.initializeConnections(dbuser, dbpassword, dbhost, dbname, port)

# test saveAccountDateLocationScores
test_that("test saveAccountDateLocationScores", {
  # run saveAccountDateLocationScores
  RUN_UID <- "RUN_UID"
  BUILD_UID <- "BUILD_UID"
  predictRundate <- as.Date(readModuleConfig(homedir, 'anchor','rundate'))
  load(sprintf('%s/anchor/tests/data/from_calculateAccountDateLocationScores_result.RData',homedir))
  requiredMockDataList <- list(pfizerusdev_learning=c('AccountDateLocationScores'))
  resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,dbname_cs,requiredMockDataList)
  saveAccountDateLocationScores(accountDateLocationScores, RUN_UID, BUILD_UID, predictRundate)

  # run tests on DB result
  accountDateLocationScores_new <- data.table(dbGetQuery(con_l, "SELECT * FROM AccountDateLocationScores;"))
  # test cases
  # test dimension
  expect_equal(dim(accountDateLocationScores_new), c(3661,13))
  # test entry in accountDateLocationScores
  accountDateLocationScores$facilityDoWScore <- as.double(accountDateLocationScores$facilityDoWScore)
  accountDateLocationScores$facilityPoDScore <- as.double(accountDateLocationScores$facilityPoDScore)
  expect_equal(accountDateLocationScores_new[order(accountId, facilityId, dayOfWeek, periodOfDay),names(accountDateLocationScores),with=F], accountDateLocationScores[order(accountId, facilityId, dayOfWeek, periodOfDay),])
  expect_equal(unique(accountDateLocationScores_new$learningRunUID),RUN_UID)
  expect_equal(unique(accountDateLocationScores_new$learningBuildUID),BUILD_UID)
  expect_equal(unique(accountDateLocationScores_new$runDate),as.character(predictRundate))
})

# test for isNighlty=TRUE
test_that("test saveAccountDateLocation for nightly run", {
  # run saveAnchorResult
  isNightly <- TRUE
  RUN_UID <- "RUN_UID"
  BUILD_UID <- "BUILD_UID"
  load(sprintf('%s/anchor/tests/data/from_processAccountDateLocationResult.RData',homedir))
  requiredMockDataList <- list(pfizerusdev_learning=c('AccountDateLocation'),pfizerusdev=c('AccountDateLocation'))
  resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,dbname_cs,requiredMockDataList)
  #accountDateLocation[, c("learningRunUID","learningBuildUID","createdAt","updatedAt"):=NULL]  # remove two columns
  #save(accountDateLocation, file=sprintf('%s/testdir/adl.RData',homedir))
  saveAccountDateLocation(accountDateLocation, RUN_UID, BUILD_UID, isNightly)

  # run tests on DB result
  accountDateLocation_dse <- data.table(dbGetQuery(con, "SELECT * FROM AccountDateLocation;"))
  accountDateLocation_l <- data.table(dbGetQuery(con_l, "SELECT * FROM AccountDateLocation;"))
  # test cases
  # test dimension
  expect_equal(dim(accountDateLocation_dse), c(1861,7))
  expect_equal(dim(accountDateLocation_l), c(0,11))
  # test entry in accountDateLocation
  accountDateLocation_dse$date <- as.Date(accountDateLocation_dse$date)
  expect_equal(accountDateLocation_dse[order(accountId,facilityId,date),c("accountId","facilityId","latitude","longitude","date")], accountDateLocation[order(accountId,facilityId,date),c("accountId","facilityId","latitude","longitude","date")])
  expect_equal(unique(accountDateLocation_dse$learningRunUID),RUN_UID)
})

# test for isNightly=FALSE
test_that("test saveAccountDateLocation for manual run", {
  # run saveAnchorResult
  isNightly <- FALSE
  RUN_UID <- "RUN_UID"
  BUILD_UID <- "BUILD_UID"
  load(sprintf('%s/anchor/tests/data/from_processAccountDateLocationResult.RData',homedir))
  requiredMockDataList <- list(pfizerusdev_learning=c('AccountDateLocation'),pfizerusdev=c('AccountDateLocation'))
  resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,dbname_cs,requiredMockDataList)
  saveAccountDateLocation(accountDateLocation, RUN_UID, BUILD_UID, isNightly)

  # run tests on DB result
  accountDateLocation_dse <- data.table(dbGetQuery(con, "SELECT * FROM AccountDateLocation;"))
  accountDateLocation_l <- data.table(dbGetQuery(con_l, "SELECT * FROM AccountDateLocation;"))
  # test cases
  # test dimension
  expect_equal(dim(accountDateLocation_dse), c(0,7))
  expect_equal(dim(accountDateLocation_l), c(1861,11))
  # test entry in accountDateLocation
  accountDateLocation_l$date <- as.Date(accountDateLocation_l$date)
  accountDateLocation_l$runDate <- as.Date(accountDateLocation_l$runDate)
  expect_equal(accountDateLocation_l[order(accountId,facilityId,date),c("accountId","facilityId","latitude","longitude","date","facilityDoWScore","runDate")], accountDateLocation[order(accountId,facilityId,date),c("accountId","facilityId","latitude","longitude","date","facilityDoWScore","runDate")])
  expect_equal(unique(accountDateLocation_l$learningRunUID),RUN_UID)
  expect_equal(unique(accountDateLocation_l$learningBuildUID),BUILD_UID)
})

# close DB connection
dataAccessLayer.common.closeConnections()
